import React from 'react';
import Card from '../UI/Card';
import Button from '../UI/Button';
import Styles from './AddUser.module.css';

const AddUser = props => {

    const addUserHandler = (event) => {
        event.preventDefault();
    }

    return (<Card className={Styles.input}>
        <form onSubmit={addUserHandler}>
            <label htmlFor="username">Username</label>
            <input id="username" type="text"></input>
            <label htmlFor="age">Age</label>
            <input type="number" id="age"></input>
            <Button type="submit">Add User</Button>
        </form>
        </Card>);
};


export default AddUser;